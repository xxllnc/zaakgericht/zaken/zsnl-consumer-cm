# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

subject_relation_prefix = (
    "zsnl.v2.zsnl_domains_case_management.SubjectRelation."
)
case_prefix = "zsnl.v2.zsnl_domains_case_management.Case."
case_legacy_prefix = "zsnl.v2.legacy.Case."
timeline_export_prefix = "zsnl.v2.zsnl_domains_case_management.TimelineExport."
